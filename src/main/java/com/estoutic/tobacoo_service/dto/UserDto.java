package com.estoutic.tobacoo_service.dto;

import com.estoutic.tobacoo_service.db.entity.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDto {

    private Long id;

    private String firstName;

    private String surName;

    private String lastName;

    private String phone;

    private String password;

    private int bonusCount;

    public UserDto(User user) {
        this.id = user.getId();
        this.firstName = user.getName();
        this.surName = user.getSurName();
        this.lastName = user.getLastName();
        this.phone = user.getPhone();
        this.bonusCount = user.getBonusCount();
    }

    public UserDto(){

    }
}