package com.estoutic.tobacoo_service.dto.enums;

import java.util.ArrayList;
import java.util.List;

public enum CategoryType {

    DISPOSABLE_DEVICE("Одноразовые устройства"),
    LIQUID("Жидкости"),
    POD("Поды");

    private final String value;

    CategoryType(String value) {
        this.value = value;
    }

    public String get() {
        return value;
    }

    public static List<CategoryType> getAllCategoryTypes() {
        return List.of(DISPOSABLE_DEVICE,LIQUID,POD);
    }
}
