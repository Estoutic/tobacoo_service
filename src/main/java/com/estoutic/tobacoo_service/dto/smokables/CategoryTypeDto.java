package com.estoutic.tobacoo_service.dto.smokables;

import com.estoutic.tobacoo_service.dto.enums.CategoryType;

public class CategoryTypeDto {

    public CategoryType categoryType;

    public String categoryName;

    public CategoryTypeDto(CategoryType categoryType){
        this.categoryType = categoryType;
        this.categoryName = categoryType.get();
    }
}
