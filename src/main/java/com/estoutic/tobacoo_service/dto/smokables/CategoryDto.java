package com.estoutic.tobacoo_service.dto.smokables;

import com.estoutic.tobacoo_service.db.entity.smokables.Category;
import com.estoutic.tobacoo_service.dto.enums.CategoryType;

public class CategoryDto {

    public String id;

    public String name;

    public CategoryType categoryType;

    public CategoryDto(Category category){
        this.id = category.getId();
        this.name = category.getName();
        this.categoryType = category.getCategoryType();
    }
}
