package com.estoutic.tobacoo_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TobacooServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(TobacooServiceApplication.class, args);
    }

}
