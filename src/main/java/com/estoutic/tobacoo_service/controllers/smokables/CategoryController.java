package com.estoutic.tobacoo_service.controllers.smokables;

import com.estoutic.tobacoo_service.dto.smokables.CategoryDto;
import com.estoutic.tobacoo_service.dto.enums.CategoryType;
import com.estoutic.tobacoo_service.dto.smokables.CategoryTypeDto;
import com.estoutic.tobacoo_service.services.ProductService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryController {

    private final ProductService productService;

    public CategoryController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/all")
    public List<CategoryTypeDto> getAllCategoryType() {
        return productService.getAllCategoriesType();
    }

    @GetMapping("")
    public CategoryDto getCategoryByType(@RequestParam(name = "categoryType") CategoryType categoryType){
        return productService.getCategoryByType(categoryType);
    }

    @PostMapping()
    public String addCategory(@RequestParam(name = "categoryName") String categoryName,
                              @RequestParam(name = "categoryType") CategoryType categoryType) {
        return productService.saveCategory(categoryName, categoryType);
    }
}
