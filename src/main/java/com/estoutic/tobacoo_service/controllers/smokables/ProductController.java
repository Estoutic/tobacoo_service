package com.estoutic.tobacoo_service.controllers.smokables;

import com.estoutic.tobacoo_service.db.entity.smokables.Product;
import com.estoutic.tobacoo_service.dto.enums.CategoryType;
import com.estoutic.tobacoo_service.dto.smokables.ProductDto;
import com.estoutic.tobacoo_service.services.ProductService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("product")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping()
    public String saveProduct(@RequestBody() ProductDto productDto) {
        return productService.saveProduct(productDto);
    }

    @GetMapping("/category")
    public List<ProductDto> findAllProducts(@RequestParam(name = "categoryType") CategoryType categoryType) {
        return productService.getAllProducts(categoryType);
    }
}
