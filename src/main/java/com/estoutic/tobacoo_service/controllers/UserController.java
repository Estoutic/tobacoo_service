package com.estoutic.tobacoo_service.controllers;

import com.estoutic.tobacoo_service.dto.UserDto;
import com.estoutic.tobacoo_service.services.UserService;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/register")
    public Integer registration(@Valid @RequestBody() UserDto userDto){
        if ( userDto == null || Objects.equals(userDto.getPhone(), "")){
            throw new RuntimeException("empty form data");
        }
        return  Math.toIntExact(userService.saveUser(userDto));
    }

    @GetMapping("/{id}")
    public UserDto getUser(@PathVariable(name= "id") String id){
        return userService.getUser(id);
    }

    @GetMapping("/login")
    public Integer getUserByPhone(@RequestParam(name = "phone") String phone,@RequestParam(name = "password") String password){
        return Math.toIntExact(userService.getUserByPhone(phone,password ));
    }

    @PatchMapping("/bonus/{id}")
    public void updateBonusStatus(@PathVariable String id,@RequestParam int bonus){
        userService.updateBonusCount(id,bonus);
    }
}
