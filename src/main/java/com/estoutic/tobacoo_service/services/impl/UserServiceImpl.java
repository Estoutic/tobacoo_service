package com.estoutic.tobacoo_service.services.impl;

import com.estoutic.tobacoo_service.db.entity.User;
import com.estoutic.tobacoo_service.db.repositories.UserRepository;
import com.estoutic.tobacoo_service.dto.UserDto;
import com.estoutic.tobacoo_service.services.UserService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;


    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;

        this.passwordEncoder = passwordEncoder;
    }


    @Override
    public Long saveUser(UserDto userDto) {

        User existingUser = userRepository.findByPhone(userDto.getPhone());

        if (existingUser != null) {
            throw new RuntimeException("user already exist");
        }
        User user = new User(userDto,passwordEncoder.encode(userDto.getPassword()));
        userRepository.save(user);

        return user.getId();
    }

    @Override
    public UserDto getUser(String id) {
        User user = userRepository
                .findById(Long.valueOf(id)).orElseThrow(() -> new RuntimeException("Account doesnt exist"));
        return new UserDto(user);
    }

    @Override
    public Long getUserByPhone(String phone, String password) {
        User user = userRepository.findByPhone(phone);
        boolean isPasswordMatched = false;
        if (user != null) {
            isPasswordMatched = passwordEncoder.matches(password, user.getPassword());
        }
        if (!isPasswordMatched) {
            return null;
        }
        return user.getId();
    }

    @Override
    public void updateBonusCount(String id, int bonus) {
        User user = userRepository
                .findById(Long.valueOf(id)).orElseThrow(() -> new RuntimeException("Account does not exist"));

        user.setBonusCount(user.getBonusCount() + bonus);
        userRepository.save(user);
    }
}
