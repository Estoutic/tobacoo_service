package com.estoutic.tobacoo_service.services.impl;

import com.estoutic.tobacoo_service.db.entity.smokables.Category;
import com.estoutic.tobacoo_service.db.entity.smokables.Product;
import com.estoutic.tobacoo_service.db.repositories.smokables.CategoryRepository;
import com.estoutic.tobacoo_service.db.repositories.smokables.ProductRepository;
import com.estoutic.tobacoo_service.dto.smokables.CategoryDto;
import com.estoutic.tobacoo_service.dto.enums.CategoryType;
import com.estoutic.tobacoo_service.dto.smokables.CategoryTypeDto;
import com.estoutic.tobacoo_service.dto.smokables.ProductDto;
import com.estoutic.tobacoo_service.services.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ProductServiceImpl implements ProductService {

    private final CategoryRepository categoryRepository;
    private final ProductRepository productRepository;

    public ProductServiceImpl(CategoryRepository categoryRepository, ProductRepository productRepository) {
        this.categoryRepository = categoryRepository;
        this.productRepository = productRepository;
    }

    @Override
    public String saveCategory(String categoryName, CategoryType categoryType) {
        if (categoryRepository.existsByName(categoryName)) {
            throw new RuntimeException("category already exist");
        }
        Category category = new Category(categoryName, categoryType);
        categoryRepository.save(category);
        return category.getId();
    }

    @Override
    public List<CategoryTypeDto> getAllCategoriesType() {
        return CategoryType.getAllCategoryTypes().stream().map(CategoryTypeDto::new).toList();
    }

    @Override
    public String saveProduct(ProductDto productDto) {
        if (productRepository.existsByName(productDto.getName())) {
            throw new RuntimeException("Product already exist");
        }
        Category category = categoryRepository.findById(productDto.getCategoryId())
                .orElseThrow(() -> new RuntimeException("Category does not exist"));
        Product product = new Product(productDto, category);
        productRepository.save(product);

        return product.getId();
    }

    @Override
    public CategoryDto getCategoryByType(CategoryType categoryType) {
        return new CategoryDto(categoryRepository.findByCategoryType(categoryType));
    }

    @Override
    public List<ProductDto> getAllProducts(CategoryType categoryType) {
        return productRepository.findAllByCategory(categoryRepository.findByCategoryType(categoryType)).stream().map(ProductDto::new).toList();
    }
}
