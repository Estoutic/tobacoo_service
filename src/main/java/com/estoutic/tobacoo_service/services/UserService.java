package com.estoutic.tobacoo_service.services;

import com.estoutic.tobacoo_service.dto.UserDto;

public interface UserService {

    Long saveUser(UserDto userDto);

    UserDto getUser(String id);

    Long getUserByPhone(String phone, String password);

    void updateBonusCount(String id, int bonus);
}
