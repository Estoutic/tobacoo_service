package com.estoutic.tobacoo_service.services;

import com.estoutic.tobacoo_service.db.entity.smokables.Product;
import com.estoutic.tobacoo_service.dto.smokables.CategoryDto;
import com.estoutic.tobacoo_service.dto.enums.CategoryType;
import com.estoutic.tobacoo_service.dto.smokables.CategoryTypeDto;
import com.estoutic.tobacoo_service.dto.smokables.ProductDto;

import java.util.List;

public interface ProductService {

    String saveCategory(String categoryName, CategoryType categoryType);

    List<CategoryTypeDto> getAllCategoriesType();

    String saveProduct(ProductDto productDto);

    CategoryDto getCategoryByType(CategoryType categoryType);

    List<ProductDto> getAllProducts(CategoryType categoryType);
}
