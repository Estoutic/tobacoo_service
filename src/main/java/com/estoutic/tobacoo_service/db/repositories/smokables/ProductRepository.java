package com.estoutic.tobacoo_service.db.repositories.smokables;

import com.estoutic.tobacoo_service.db.entity.smokables.Category;
import com.estoutic.tobacoo_service.db.entity.smokables.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, String> {

    boolean existsByName(String name);

    List<Product> findAllByCategory(Category category);
}
