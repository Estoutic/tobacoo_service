package com.estoutic.tobacoo_service.db.repositories;

import com.estoutic.tobacoo_service.db.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByPhone(String phone);

    User findByPhoneAndPassword(String phone,String password);
}
