package com.estoutic.tobacoo_service.db.repositories.smokables;

import com.estoutic.tobacoo_service.db.entity.smokables.Category;
import com.estoutic.tobacoo_service.dto.enums.CategoryType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category,String> {

    Boolean existsByName(String name);

    Category findByCategoryType (CategoryType categoryType);
}
