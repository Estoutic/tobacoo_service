package com.estoutic.tobacoo_service.db.entity;

import com.estoutic.tobacoo_service.dto.UserDto;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users")
public class User {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable=false)
    private String name;

    @Column(nullable=false)
    private String surName;

    @Column(nullable=false)
    private String lastName;

    @Column(nullable=false, unique=true)
    private String phone;

    @Column(nullable=false)
    private String password;

    @Column(nullable=false)
    private int bonusCount;

    public User(UserDto userDto, String password) {
        this.name = userDto.getFirstName();
        this.surName = userDto.getSurName();
        this.lastName = userDto.getLastName();
        this.phone = userDto.getPhone();
        this.password = password;
        this.bonusCount = userDto.getBonusCount();
    }
}
